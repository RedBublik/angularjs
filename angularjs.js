


var someVar = 'text from js';

var AngularModule = (function(){

	

	function getHtml(item){
		var xhttp = new XMLHttpRequest();
		xhttp.open('GET',item.getAttribute('data-get-html'), true);
		xhttp.send();

		xhttp.onreadystatechange = function(){
			if (xhttp.readyState === 4){
				if(xhttp.status === 200){
					item.innerHTML = templaterHtml(xhttp.responseText);
					getHtmlPage(item);
				}else if(xhttp.status === 404){
					item.innerHTML = "404 " + xhttp.statusText;
				}
				
			}
		}; 
	};

	function getHtmlPage(node){
		var arr = node.querySelectorAll('[data-get-html]');

		[].forEach.call(arr, function(item){
			getHtml(item);
		});
	};

	function templaterHtml(html){

		var __regexp = /{(.*)}/g;
		var __dom = html;
		var __myArray;

		while ((__myArray = __regexp.exec(__dom)) != null)
		{   
		    try {

		   		__dom = __dom.replace(__myArray[0], eval(__myArray[1]));

			} catch (err) {

			  	__dom = __dom.replace(__myArray[0], '');

			}
		}
		// document.documentElement.innerHTML = __dom;
		return __dom;
	}

	return {
		getHtml: getHtmlPage,
		templater: templaterHtml,

	}

}());

document.documentElement.innerHTML = AngularModule.templater(document.documentElement.innerHTML);
AngularModule.getHtml(document);

